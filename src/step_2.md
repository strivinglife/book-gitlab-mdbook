# Step 2: Adding content
If you have mdBook up and running, you can add new content (pages) by editing the SUMMARY.md file.

This summary acts as the table of contents, and if you include a link to a file that doesn't exist it will automatically be created.

For example, you could add a new Introduction page by updating the SUMMARY.md to the following.

```markdown
# Summary

- [Introduction](./intro.md)
- [Chapter 1](./chapter_1.md)
```

This will automatically create a new `intro.md` file with the title of Introduction. This will also update the table of contents and allow you to move between the Introduction and Chapter 1 pages by way of arrows on screen, and on the keyboard.

```bash
git add .
git commit -m "Add message summary"
```

## Manually adding a page
You can also manually add a page by creating a new Markdown file and then updating the SUMMARY.md file to point to it. You can nest files in folders, as well as create subchapters.

See [SUMMARY.md in the mdBook Documentation](https://rust-lang.github.io/mdBook/format/summary.html) for more information.
