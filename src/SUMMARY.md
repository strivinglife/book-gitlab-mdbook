# Summary

- [Introduction](./intro.md)
- [Step 0: Dependencies](./step_0.md)
- [Step 1: Getting started](./step_1.md)
- [Step 2: Adding content](./step_2.md)
- [Step 3: Editing content](./step_3.md)
- [Step 4: GitLab repository](./step_4.md)
- [Step 5: Book TOML](./tweaks/toml.md)
- [Step 6: gitlab-ci](./step_6.md)
- [Step 7: Viewing your mdBook on GitLab Pages](./step_7.md)
- [Step 8: Wrapping up](./wrapping-up.md)
