# Step 8: Wrapping up
With that, you've now created a mdBook and have it hosted for free on GitLab Pages.

You can make edits to the book both locally as well as on GitLab itself by editing or creating new pages. Once you commit your changes GitLab will automatically generate an updated site.

See [the official mdBook Documentation](https://rust-lang.github.io/mdBook/index.html) for more information and advanced techniques.
