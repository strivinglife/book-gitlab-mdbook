# Step 1: Getting started
This will assume that you have mdBook installed, but you can easily create and edit a mdBook without the CLI.

These commands have been run in PowerShell on Windows, but will be similar on other environments.

## Create a new book
Create a new book by running one of the following commands:
```powershell
# Create a new book in the current directory.
.\mdbook.exe init
# Create a book in the specified directory.
.\mdbook.exe init book-gitlab-mdbook
```

You'll be prompted on whether you want to create a .gitignore file - `y` - and the title of your book.

`cd` into the new book directory, if you created one, and start up Visual Studio Code (`code .`) or the editor of your choice.

We can also start the book by running the `serve` command.

```powershell
..\mdbook.exe serve -o
.\mdbook.exe serve -o .\book-gitlab-mdbook\
```

This book will have the title we supplied, a single chapter (Chapter 1), and functionality to change the theme, search, and view and print-friendly version of the entire book.

## Add Git
Since we're going to have GitLab host the book for us we'll need to add Git. From the new book's root directory (`book-gitlab-mdbook` for example):

```bash
git init
git add .
# Whatever message you want to use.
git commit -m "Initialize mdBook"
```
