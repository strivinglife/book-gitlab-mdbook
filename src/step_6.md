# Step 6: gitlab-ci
Once you have your book in a fairly good state, or are otherwise ready to start publishing it to GitLab Pages, you can create a GitLab CI file.

In the root of your book's directory create a new .gitlab-ci.yml file with the following contents:

```yml
stages:
    - deploy

pages:
  stage: deploy
  image: rust:latest
  variables:
    CARGO_HOME: $CI_PROJECT_DIR/cargo
  before_script:
    - export PATH="$PATH:$CARGO_HOME/bin"
    - mdbook --version || cargo install --debug mdbook
  script:
        - mdbook build -d public
  only:
      - main
  artifacts:
      paths:
          - public
  cache:
    paths:
    - $CARGO_HOME/bin
```

> Update the branch in pages > only as needed.

Using the latest Rust image, this will setup Cargo, make sure mdBook is installed, and then use it to build the book to a public directory. This public directory will then be used by GitLab Pages.

```bash
git add .
git commit -m "Add GitLab CI YML configuration"
git push
```
