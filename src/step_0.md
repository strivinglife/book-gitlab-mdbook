# Step 0: Dependencies
If you want to use the mdBook utility to create and develop books locally you can find installation instructions on the official GitHub code repository.

- [mdBook on GitHub](https://github.com/rust-lang/mdBook)
