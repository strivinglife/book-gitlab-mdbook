# Step 3: Editing content
If you want to edit content simply open the Markdown file you want to change and edit the content.

If you still have `mdbook.exe serve` up and running, the browser will automatically be updated when you save the content.

If you want to change the file name of a page, your best bet is to update the SUMMARY.md file and then move your content over. Alternatively you can stop mdbook.exe and it won't helpfully create files that don't exist.

```bash
git add .
git commit -m "Update message summary"
```
