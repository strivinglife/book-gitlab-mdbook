# Step 5: Book TOML
The book.toml file contains basic information about your book.

You can generally get by fine with the default contents, but you may want to tweak the file.

## output.html
Depending upon the audience of your book, you may want to link to the Git repository.

The following two changes, added to the end of the book.toml file, will add a link to the Git repository and the page to edit the Markdown file that generates the current page.

> Update the repository URL and branch name, in `edit-url-template`, as needed.

```toml
[output.html]
git-repository-url = "https://gitlab.com/strivinglife/book-gitlab-mdbook"
edit-url-template = "https://gitlab.com/strivinglife/book-gitlab-mdbook/-/edit/main/{path}"
git-repository-icon = "fa-gitlab"
```

> The Git repository URL will use the GitHub icon by default, even if you link to a non-GitHub page. `git-repository-icon` can be used to override that default, as shown above.

There are other options, detailed in the [Configuration section of the mdBook Documentation](https://rust-lang.github.io/mdBook/format/configuration/index.html).
