# Step 4: GitLab repository
Since we're going to use GitLab to host the site via [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), we'll need to create a new repository for the book.

> You could have optionally done this as part of the initial setup, and then cloned the new repository and initialized mdBook.

Log into GitLab and create a new project/repository.

Since we already initialized the Git repo locally we can create a blank project.

> Alternatively, you could also use GitHub as your primary source and then use the Run CI/CD for external repository option here. This option works best if the repository is public.

Give the project a name, update the project slug if needed, provide a project description if you'd like, and set the (code) visibility level. If you've run `git init` locally you'll want to make sure you uncheck the "Initialize repository with a README" option.

You'll now have a GitLab repository that we can hook up to our local repo.

## Adding the remote to our local repo
We can use a combination of the commands under the "Push an existing folder" and "Push an existing Git repository" headings that GitLab provides to update our local repo and push it to GitLab.

Update the URL (`https://gitlab.com/strivinglife/book-gitlab-mdbook.git`) and branch name (`main`) as needed.

```bash
git remote add origin https://gitlab.com/strivinglife/book-gitlab-mdbook.git
git push -u origin main
```
