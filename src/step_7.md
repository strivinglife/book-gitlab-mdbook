# Step 7: Viewing your mdBook on GitLab Pages
Once you've published your .gitlab-ci.yml file, or have submitted a change, you'll be able to see the status of the new deployment on both the repository main page and the CI/CD > Pipelines page.

Assuming everything is setup correctly you can navigate to Settings > Pages in GitLab and you'll see the URL of the mdBook you've just created. In this case it's at https://strivinglife.gitlab.io/book-gitlab-mdbook/.

This is also where you can setup a custom domain if you have and would like to use one.
